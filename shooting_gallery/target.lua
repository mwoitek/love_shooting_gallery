local function Target()
  local self = { radius = 50 }

  local x_max = love.graphics.getWidth() - self.radius
  local y_max = love.graphics.getHeight() - self.radius

  function self.set_random_position()
    self.x = math.random(self.radius, x_max)
    self.y = math.random(self.radius, y_max)
  end

  self.set_random_position()
  return self
end

return Target
