local MAX_TIME = 60
local CUSTOM_RED = { 0.9961, 0.0863, 0.0863 }

local is_playing = false
local score = 0
local timer = 0

local fonts
local sprites
local target

local ceil = math.ceil
local str_format = string.format

---@type function
local draw
---@type function
local new_text

math.randomseed(os.time())

function love.load()
  fonts = {
    western_small = love.graphics.newFont("fonts/NandakaWestern.ttf", 36),
    western_big = love.graphics.newFont("fonts/NandakaWestern.ttf", 54),
  }
  sprites = {
    wood = love.graphics.newImage "sprites/wood.png",
    crosshairs = love.graphics.newImage "sprites/crosshairs.png",
    target = love.graphics.newImage "sprites/target.png",
  }

  local Target = require "target"
  target = Target()

  draw = love.graphics.draw
  new_text = love.graphics.newText

  love.graphics.setColor(1, 1, 1)
  love.mouse.setVisible(false)
end

function love.update(dt)
  if timer > 0 then timer = timer - dt end

  if timer < 0 then
    timer = 0
    is_playing = false
  end
end

function love.draw()
  draw(sprites.wood, 0, 0)

  local score_text =
    new_text(fonts.western_small, { CUSTOM_RED, str_format("Score: %02d", score) })
  draw(score_text, 5, 5)

  if is_playing then
    local time_text =
      new_text(fonts.western_small, { CUSTOM_RED, str_format("Time: %02d", ceil(timer)) })
    draw(time_text, 840, 5)
    draw(sprites.target, target.x, target.y, nil, nil, nil, target.radius, target.radius)
  else
    local begin_text =
      new_text(fonts.western_big, { CUSTOM_RED, "Click anywhere to begin" })
    draw(begin_text, 150, 373)
  end

  draw(sprites.crosshairs, love.mouse.getX(), love.mouse.getY(), nil, nil, nil, 20, 20)
end

local function distance(x1, y1, x2, y2)
  return math.sqrt((x2 - x1) ^ 2 + (y2 - y1) ^ 2)
end

function love.mousepressed(x, y, button)
  if button ~= 1 then return end

  if is_playing then
    local distance_to_target = distance(x, y, target.x, target.y)
    if distance_to_target <= target.radius then
      score = score + 1
      target.set_random_position()
    end
  else
    is_playing = true
    score = 0
    timer = MAX_TIME
  end
end
